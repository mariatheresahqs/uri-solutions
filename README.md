# URI-Solutions

My solutions to problems from www.urionlinejudge.com.br.

The solutions are mainly in C, with a few in Python.

# About URI Online Judge

The URI Online Judge is a project that is being developed by the Computer Science Department of URI University. The main goal of the project is to provide programming practice and knowledge sharing. The URI Online Judge contains more than 1,000 problems divided in 8 big categories. This division help the users to focus on specific programming topics. All problems are available in Portuguese and English. The URI Online Judge website also has public contests on a regular basis.